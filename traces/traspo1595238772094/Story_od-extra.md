# Story od-extra

## Given

https://www.trentinotrasporti.it/open-data

Dati del trasporto pubblico urbano e extraurbano in formato GTFS. Aggiornamento quindicinale. Principali informazioni disponibili: anagrafica delle fermate (georeferenziate), elenco delle linee, elenco delle corse, elenco degli orari di arrivo e partenza.I file zip pubblicati sono conformi alle specifiche GTFS (General Transit Feed Specification). I dettagli del formato sono disponibili a questo indirizzo: https://developers.google.com/transit/gtfs/

Tutti i dati che mettiamo a disposizione sono stati concessi con licenza CC BY 2.5
https://creativecommons.org/licenses/by/2.5/it/

---
## When

Copertura Temporale (Data di inizio)	30/05/2020

Copertura Temporale (Data di fine)	13/09/2020

Aggiornamento	Quindicinale

Data di creazione	02/07/2020

Data di pubblicazione	02/07/2020

Data di aggiornamento	02/07/2020

---
## Who

Titolare	Trentino Trasporti S.p.A.

Copertura Geografica	Trento

Origine	Trentino Trasporti S.p.A.

Autore	Trentino Trasporti S.p.A.

Referente	Trentino Trasporti S.p.A. - info@trentinotrasporti.it 

---
## What

Codifica Caratteri	UTF-8

[classes](http://localhost:7000/chart/classes/traspo1595238772094/traspo1595238772094_db.xml)

### service id

Il service_id è un numero univoco e descrive la durata di un servizio data inizio e data fine e la sua “frequenza” in che giorni della settimana cade

Nel file calendar_date.txt gestiamo le eccezioni


---
## Examples
Un servizio che inizia 10/08/20 e finisce il 22/08/20 va da lunedì a sabato e dobbiamo escludere 15/08 che è Ferragosto

### Calendar.txt
<pre>
service_id, mon,tue,wed,thu,fri,sat,sun,  start_date,end_date
00000001,   1,  1,  1  ,1  ,1  ,1  ,0  ,  20200810,   20200822
</pre>

### Calendar_date.txt
 <pre>
service_id,date,exception_type
00000001,20200815,2
 </pre>
---
## Doubs

<pre>
select * from routes where route_long_name LIKE '%corona%' limit 40
</pre>
duplicated results?
<pre>
-----------------------------------------------------ROUTE_ID: 345
AGENCY_ID: 12
ROUTE_SHORT_NAME: B611
ROUTE_LONG_NAME: Trento-Mezzocorona-Fai d/Paganella-Andalo-Molveno
ROUTE_TYPE: 3
-----------------------------------------------------ROUTE_ID: 658
AGENCY_ID: 12
ROUTE_SHORT_NAME: B611
ROUTE_LONG_NAME: Tn-Mezzocorona-Fai-Cavedago-Andalo-Molveno
ROUTE_TYPE: 3
</pre>

---
<pre>
select (*) from trips where route_id='345' 
                       and direction_id='1' limit 400
</pre>
42

---
<pre>
select * from stopslevel  limit 400
</pre>
cosa sono i numeri di  STOP_LEVEL ?